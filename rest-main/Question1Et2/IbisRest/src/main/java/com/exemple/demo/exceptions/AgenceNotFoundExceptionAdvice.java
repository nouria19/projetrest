package com.exemple.demo.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
@ControllerAdvice
public class AgenceNotFoundExceptionAdvice extends Exception {
	@ExceptionHandler(AgenceNotFoundExceptionAdvice.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String employeeNotFoundExceptionHandler(AgenceNotFoundExceptionAdvice e) {
	return String.format("{\"%s\": \"%s\"}", "error", e.getMessage());
	}
}
