package com.exemple.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exemple.demo.models.Offre;



public interface OffreRepository  extends JpaRepository<Offre,Long>{

}
