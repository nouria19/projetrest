package com.exemple.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exemple.demo.models.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation,String>{

}
