package com.exemple.demo.controllers;


import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.exemple.demo.exceptions.AgenceNotFoundException;

import com.exemple.demo.models.Agence;
import com.exemple.demo.models.Hotel;
import com.exemple.demo.models.Offre;
import com.exemple.demo.repository.AgenceRepository;
import com.exemple.demo.repository.HotelRepository;
import com.exemple.demo.repository.OffreRepository;
import org.springframework.http.HttpStatus;

//pour consommer on passe par l'url puis on fait appels au méthodes qui sont déclarer dans le HotelController

//Web service Rest pour la Consultation Offre
@RestController
public class HotelController {
	@Autowired //permet de faire de l'injection de dépendance
	private HotelRepository repository;
	private static final String uri ="/Ibis/Consultation/api";
	private  AgenceRepository repo;
	@Autowired
    private OffreRepository rep;
	/*
	/* METHODS */
	@GetMapping(uri+"/hotels")
	public Hotel getHotel(){
	return repository.findAll().get(0);
	}
	
	@GetMapping(uri+"/Hotel/{idH}")
	public Optional<Hotel> getHotelId(Long id) {
		return repository.findById(id);
	}
	
	@DeleteMapping(uri+"/DeleteOffre")
	public void DeleteOffre() {
		rep.deleteAll();
	}
	
	
	@PostMapping(uri+"/ListOffre/{idP}/{pass_word}/{NB_personne}/{dateD}")
 public List<Offre> ListOffre(@PathVariable int idP, @PathVariable String pass_word,@PathVariable int NB_personne,@PathVariable String dateD) throws AgenceNotFoundException {
		
		if(rep.findAll().size() > 0) {
			DeleteOffre();
		}
		 SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			Date  d = sdformat.parse(dateD);
		
		for (int i=0;i<repository.findById(1L).get().getChambres().size();i++) {
			if(PartOfH(idP,pass_word) && NB_personne <= repository.findById(1L).get().getChambres().get(i).getNb_lits() && d.compareTo(repository.findById(1L).get().getChambres().get(i).getDateDispo()) > 0
					) {
				Offre o = new Offre(repository.findById(1L).get().getChambres().get(i));
				rep.save(o);
			}	
		}
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
	if(rep.findAll() == null) {
	 new AgenceNotFoundException("Agence n'existe pas");
	}
   return rep.findAll();
		
	}

	
	public boolean PartOfH(int idP, String pass_word) throws AgenceNotFoundException {
		
		 List<Agence> tabAgences = repository.findById(1L).get().getAgences();
      
            
           	 for(Agence e:tabAgences) {
				if( pass_word.equals(e.getPass_word()) && e.getIdP()==idP)
				 {
				  
				     return true;
				 }else new AgenceNotFoundException("Agence n'existe pas ");
				}	
           	 return false;
		
	}
	
	
	}
