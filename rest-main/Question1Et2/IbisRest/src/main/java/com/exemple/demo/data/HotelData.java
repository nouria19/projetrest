package com.exemple.demo.data;


import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.exemple.demo.models.Agence;
import com.exemple.demo.models.Chambre;
import com.exemple.demo.models.Hotel;
import com.exemple.demo.models.Offre;
import com.exemple.demo.repository.AgenceRepository;
import com.exemple.demo.repository.ChambreRepository;
import com.exemple.demo.repository.HotelRepository;
import com.exemple.demo.repository.OffreRepository;



@Configuration
public class HotelData{
private Logger logger = LoggerFactory.getLogger(this.getClass());

@Bean
    public CommandLineRunner initDatabase(AgenceRepository repository1) {
    return args -> {//c'est les données à inserer à la base de données
    logger.info("Remplir la database avec "+ repository1.save(
    new Agence("agence6", false, 0.1f,"nouria1","Voyager smart")));
    logger.info("Remplir la database avec "+ repository1.save(
    new Agence("agence2", false,0.2f,"Djou","AventuresFILA")));
    logger.info("Remplir la database avec "+ repository1.save(
    new Agence("agence4", false, 0.4f,"Dihia3","Voyager économique")));
    };
}



@Bean
    public CommandLineRunner initDatabase1(ChambreRepository repository) throws URISyntaxException {
 SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
 URI url1= new URI("https://www.voyagetips.com/wp-content/uploads/2018/08/Hotel-Negresco.jpg");
 URI url2 = new URI("https://www.usinenouvelle.com/mediatheque/5/8/2/000787285_896x598_c.jpg");
 URI url3 = new URI("https://plagepalace.com/wp-content/uploads/2019/07/HOTEL_04_Plage-Palace.png");
 URI url4 = new URI("https://www.hotelarmoniparis.com/_novaimg/galleria/967131.jpg");
 
    return args -> {//c'est les données à inserer à la base de données
   
    logger.info("Remplir la database avec "+ repository.save(new Chambre(1,4,1225,sdformat.parse("2022-02-13"), true,url1)));
    logger.info("Remplir la database avec "+ repository.save(new Chambre(10,4,5587,sdformat.parse("2022-01-25"), true,url2)));
    logger.info("Remplir la database avec "+ repository.save( new Chambre(2,2,1027,sdformat.parse("2022-10-05"), true,url3)));
    logger.info("Remplir la database avec "+ repository.save( new Chambre(18,1,1000,sdformat.parse("2022-12-10"), true,url4)));
    };
}
@Bean
    public CommandLineRunner initDatabase2(HotelRepository repository2, AgenceRepository repo,ChambreRepository repoCh) {
	List<Chambre> ch = new ArrayList<>();
	List<Agence> A = new ArrayList<>();
	return args -> {//c'est les données à inserer à la base de données
    	Agence agence =  repo.findById(1L).get();
    	Agence agence1 =  repo.findById(2L).get();
    	Agence agence2 =  repo.findById(3L).get();
    	
    	A.add(agence);
    	A.add(agence1);
    	A.add(agence2);
    
    	Chambre cha = repoCh.findById(1L).get();
    	Chambre cha1 = repoCh.findById(2L).get();
    	Chambre cha2 = repoCh.findById(3L).get();
    	Chambre cha3 = repoCh.findById(4L).get();
    	    	
    	ch.add(cha);
    	ch.add(cha1);
    	ch.add(cha2);
    	ch.add(cha3);
    	
  logger.info("Remplir la database avec "+ repository2.save(new Hotel("Ibis","France","Montpellier","Bartholdi",12,"12 Rue Bartholdi",13,5,A,ch)));
    };
}

}