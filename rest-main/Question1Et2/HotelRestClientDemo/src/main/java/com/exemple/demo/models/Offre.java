package com.exemple.demo.models;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Offre {

	private Long ident;
	  private Chambre chambre;
	    public Offre()
	    {}
		public Offre(Chambre offres) {
			this.chambre = offres;
		}
		public Long getIdentifiant() {
			return ident;
		}

		public void setIdentifiant(Long ident) {
			this.ident = ident;
		}

		public Chambre getChambre() {
			return chambre;
		}

		public void setChambre(Chambre chambre) {
			this.chambre = chambre;
		}
	    
}
