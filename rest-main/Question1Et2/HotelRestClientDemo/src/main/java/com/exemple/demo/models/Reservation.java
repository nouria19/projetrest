package com.exemple.demo.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Reservation {
	
	private String ref;
	private int nb_personnes;
	private Date dateA;
	private Date dateD;
	private Client client;
	private Chambre Chambres;
	private Agence Agence;
	
	public Reservation() {
			
		}
	public Reservation(int nb_personnes, Date datA, Date dateD,String nom,String prenom,String infoCB,Agence agence,Client client) {
		super();
		this.nb_personnes = nb_personnes;
		this.dateA = datA;
		this.dateD = dateD;
		
		Agence = agence;
		this.client=client;
		this.Agence=agence;
		
	}
	
	public Reservation(Chambre chambre, String dateDebut, String dateFin, int nombrePersonnes,
			String infoCarteCreditClient,Client client,  Agence agence2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
		try {
			this.dateD=sdf.parse(dateDebut);
			this.dateA=sdf.parse(dateFin);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.client=client;
		this.nb_personnes=nombrePersonnes;
		
	}

	public Agence getAgence() {
		return Agence;
	}

	public void setAgence(Agence agence) {
		Agence = agence;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public int getNb_personnes() {
		return nb_personnes;
	}

	public void setNb_personnes(int nb_personnes) {
		this.nb_personnes = nb_personnes;
	}

	public Date getDatA() {
		return dateA;
	}

	public void setDatA(Date datA) {
		this.dateA = datA;
	}

	public Date getDateD() {
		return dateD;
	}

	public void setDateD(Date dateD) {
		this.dateD = dateD;
	}

	public Chambre getChambres() {
		return Chambres;
	}

	public void setChambres(Chambre chambres) {
		Chambres = chambres;
	}


}
