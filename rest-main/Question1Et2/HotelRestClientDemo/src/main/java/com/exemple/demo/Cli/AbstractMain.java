package com.exemple.demo.Cli;

import java.io.BufferedReader;
import java.io.IOException;

public abstract class AbstractMain {
	public static String SERVICE_URL;
	public static String SERVICE_URL1;
	public static final String QUIT = "0";

	protected void setTestServiceUrl(BufferedReader inputReader)
	throws IOException {

	System.out.println("Please provide the first URL to the web service to consume in order to consult the offers : ");
	SERVICE_URL = inputReader.readLine();
	
	while(!validServiceUrl()) {
	System.err.println("Error: "+ SERVICE_URL+ 
	" isn't a valid web service URL. "
	+ "Please try again: ");
	SERVICE_URL = inputReader.readLine();
	
	}
	}
	
	protected void setTestServiceUrl1(BufferedReader inputReader1)
			throws IOException {

			System.out.println("Please provide the second URL to the web service to consume in order to book a room : ");
			SERVICE_URL1 = inputReader1.readLine();
			while(!validServiceUrl1()) {
			System.err.println("Error: " + SERVICE_URL1+
			" isn't a valid web service URL. "
			+ "Please try again: ");
			SERVICE_URL1 = inputReader1.readLine();
			}
			}
	
	

	protected abstract boolean validServiceUrl();
	protected abstract boolean validServiceUrl1();
	protected abstract void menu();

	
		
}
