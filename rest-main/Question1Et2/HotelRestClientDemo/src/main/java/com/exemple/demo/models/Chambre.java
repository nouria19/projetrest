package com.exemple.demo.models;

import java.net.URI;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Chambre {
	
	private Long identifiant;	
	private int num_chambre;
	private int Nb_lits;
	private float prix;
	private Date dateDispo;
	private boolean est_libre;
	private URI url;
	
public Chambre() {
		
	}
	public Chambre(int num_chambre, int nb_lits, float prix, Date date,boolean est_libre ,URI url1) {
		this.num_chambre = num_chambre;
		Nb_lits = nb_lits;
		this.prix = prix;
		this.dateDispo = date;
		this.est_libre=est_libre;
		this.url=url1;
	}

	public URI getUrl() {
		return url;
	}

	public void setUrl(URI url) {
		this.url = url;
	}

	public boolean isEst_libre() {
		return est_libre;
	}

	public void setEst_libre(boolean est_libre) {
		this.est_libre = est_libre;
	}


	public int getNum_chambre() {
		return num_chambre;
	}

	public void setNum_chambre(int num_chambre) {
		this.num_chambre = num_chambre;
	}

	public int getNb_lits() {
		return Nb_lits;
	}

	public void setNb_lits(int nb_lits) {
		Nb_lits = nb_lits;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	public Date getDateDispo() {
		return dateDispo;
	}

	public void setDateDispo(Date dateDispo) {
		this.dateDispo = dateDispo;
	}
	public Long getIdentifiant() {
	return identifiant;
	}


	public void setIdentifiant(Long identifiant) {
		this.identifiant = identifiant;
	}
	
	
	

}
