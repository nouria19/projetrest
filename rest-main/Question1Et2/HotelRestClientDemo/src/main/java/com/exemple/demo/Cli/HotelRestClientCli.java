package com.exemple.demo.Cli;

import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Map; 
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;


import com.fasterxml.jackson.*;
import com.exemple.demo.models.*;
import java.util.*;

@Component
public class HotelRestClientCli extends AbstractMain implements CommandLineRunner{
	
	/* ATTRIBUTES */
	@Autowired
	private RestTemplate proxy1;
	@Autowired
	private RestTemplate proxy2;
	private ComplexUserInputProcessor inputProcessor;
	private static String URI_Hotel; 
	private static String URI_Hotel_ID; 
	private static String URI_Agence; 
	private static String URI_Agence_ID; 
	
	//public Map<String,String> dic= new HashMap<String,String>();
	//public ArrayList<Map.Entry<String, String>> arr = new ArrayList<Map.Entry<String, String>>();
	
	
	Map<String,String> m = new HashMap<>();  
	
	@Override
	protected boolean validServiceUrl() { 
		m.put("1", "Titanic");
		m.put("2", "Ibis");
		m.put("3", "Alp_Hotel");
		for(String v :m.keySet()){
	        if(SERVICE_URL.equals("http://localhost:808"+v+"/"+m.get(v)+"/Consultation/api")) {
	        	return SERVICE_URL.equals("http://localhost:808"+v+"/"+m.get(v)+"/Consultation/api");
	        }
			}
		
		return false;
		
	}

	@Override
	protected boolean validServiceUrl1() {
		
		for(String v : m.keySet()){
			if(SERVICE_URL1.equals("http://localhost:808"+v+"/"+m.get(v)+"/Reservation/api")) {
				return SERVICE_URL1.equals("http://localhost:808"+v+"/"+m.get(v)+"/Reservation/api");
			}
			}
		
			return false;
	}

	

	@Override
	public void run(String... args) throws Exception {	
		//1er web service hotel consultation
		BufferedReader inputReader;
		String userInput = "";//c'est pour les cases 
		
		//2eme web service reservation par l'agence
		BufferedReader inputReader1;
		
		try {
		inputReader = new BufferedReader(
		new InputStreamReader(System.in));
		setTestServiceUrl(inputReader);
		URI_Hotel = SERVICE_URL;
		URI_Hotel_ID = URI_Hotel + "/{idH}";
			
		inputReader1 = new BufferedReader(
				new InputStreamReader(System.in));
		setTestServiceUrl1(inputReader1);
		
		URI_Agence = SERVICE_URL1;
		URI_Agence_ID = URI_Agence + "/{idP}";
			
		do {
		menu();
		userInput = inputReader.readLine();
		
		processUserInput(inputReader, userInput,proxy1,inputReader1,proxy2);
		Thread.sleep(3000);
		
		} while(!userInput.equals(QUIT));
		} catch (IOException e) {
		e.printStackTrace();
		} catch (InterruptedException e) {
		e.printStackTrace();
		}
		}
	
	public boolean valideLaDate(String laDate){
        if(laDate.length()==0){
            return false;
        }
        
        String dateFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);
        try {
            Date theDate = sdf.parse(laDate);
        } catch (java.text.ParseException e) {
            return false;
        }
        return true;
    }
	
	private void processUserInput(BufferedReader reader, 
            String userInput,RestTemplate proxy1,BufferedReader reader1, 
            RestTemplate proxy2) {
		
		Map<String, String> params = new HashMap<>();
		@SuppressWarnings("resource")
		Scanner scanner= new Scanner(System.in);
		
        try {
            switch(userInput) {
            case "1":
            	String uri4 =URI_Agence +"/Agences";
            	System.out.println("La liste des agences est :");
            	Agence[] agence=proxy2.getForObject(uri4,Agence[].class,null, null);
               for(int i=0;i<agence.length;i++) {
            	 System.out.println("*************************************************************************************************");
          		 System.out.println("                                                                                                 ");
          		 System.out.println("Id de l'agence: "+ agence[i].getIdP());
          		 System.out.println("le nom de l'agence : "+agence[i].getNomAgence());
          		 System.out.println("*************************************************************************************************");
               }
            	
            	break;
                case "2":
                	String uri = URI_Hotel +"/ListOffre/{idP}/{pass_word}/{NB_personne}/{dateA}";
           
            		
            		
                	System.out.println("Veuillez saisir l'id de l'agence");
                	long idP = scanner.nextLong();
                	params.put("idP", String.valueOf(idP));
                	
                	System.out.println("Veuillez saisir le pass_word de l'agence");
                	String pass_word = scanner.next();
                	params.put("pass_word", String.valueOf(pass_word));
                	int NB_personne;
                	do {
                	System.out.println("Veuillez saisir le nombre de personne à héberger supérieur ou égal à 1");
                	NB_personne = scanner.nextInt();
            		params.put("NB_personne", String.valueOf(NB_personne));
                	}while(NB_personne <= 0);
                	
            		
            		String dateA;
            		
            		do {
            		System.out.println("Veuillez saisir la date de votre arriver sous forme YYYY-MM-DD");
            		dateA = scanner.next();
            		params.put("dateA", String.valueOf(dateA));
            		}while(valideLaDate(dateA)==false);
            		
            		String dateD;
            		do {
            		System.out.println("Veuillez saisir la date de départ sous forme YYYY-MM-DD");
            		dateD = scanner.next(); 
            		params.put("dateD", String.valueOf(dateD));
            		}while(valideLaDate(dateD)==false);
                	
            		
            		Offre[] offre=proxy1.postForObject(uri,null,Offre[].class, params);
                	System.out.println("La liste d'offres est :");
                	Arrays.asList(offre);
                	 if(offre.length!=0) {
                	for(int i=0;i<offre.length;i++){
	                		 System.out.println("*************************************************************************************************");
	                		 System.out.println("                                                                                                 ");
	                		 System.out.println("Offre numero "+ offre[i].getIdentifiant());
	                		 System.out.println("Le nombre de lit de cette chambre est "+offre[i].getChambre().getNb_lits());
	                		 System.out.println("La Date de disponibilité de cette chambre est le  "+offre[i].getChambre().getDateDispo());
	                		 System.out.println("Le prix de cette chambre est "+offre[i].getChambre().getPrix());
	                		 System.out.println("L'image de la chambre "+offre[i].getChambre().getUrl());
	                		 System.out.println("                                                                                                 ");
	                		 System.out.println("*************************************************************************************************");
                	}}else {
                		   System.out.println("***************************************************************************************************");
		            	   System.out.println("                                                                                                   ");
		            	   System.err.println("Alert : Aucune des offres ne correspond à votre recherche veuillez verifier les informations saisie");
		            	   System.out.println("                                                                                                   ");
		            	   System.out.println("***************************************************************************************************");
		            	   break;
                	}
                	      
                	 System.out.println("Voulez vous reserver une chambre d'une des offres proposés.Répondez par oui ou non");
                	 String rep = scanner.next();
                	 switch(rep) {
                	 case "oui":
		                	String uri1 = URI_Agence +"/sauvegarderReservation/{idP}/{identifiantOffre}/{dateA}/{dateD}/{NB_personne}/{nomC}/{prenomC}/{infoB}";
		                	String uri2= URI_Agence + "/Reservation";
		                	System.out.println("Veuillez saisir votre nom");
		             		String nomC = scanner.next();
		             		params.put("nomC", String.valueOf(nomC));
		             		System.out.println("Veuillez saisir votre prenom");
		             		String prenomC = scanner.next();
		             		params.put("prenomC", String.valueOf(prenomC));
		             		System.out.println("Veuillez saisir votre IBAN");
		             		String infoB = scanner.next();
		             		params.put("infoB", String.valueOf(infoB));
             		
             		
            		
             		
             		System.out.println("Veuillez saisir l'offre qui vous interesse : ");
             		long identifiantOffre = scanner.nextLong();
             		params.put("identifiantOffre", String.valueOf(identifiantOffre));
             		proxy2.put(uri1,null, params);
             		
                	
                	long ch = 0L;
                	for(int i=0;i<offre.length;i++){
                		if(identifiantOffre == offre[i].getIdentifiant() ) {
                			ch = offre[i].getChambre().getIdentifiant();
                		}
                	}
                	Reservation[] R = proxy2.getForObject(uri2,Reservation[].class,null,null);
                	for(int i=0;i<R.length;i++){
                		if(R[i].getChambres().getIdentifiant() == ch) {
                			System.out.println("La reservation est bien sauvegardée :");
                			System.out.println("Le code de reservation de votre chambre est "+ R[i].getRef());
                		}
                	}
                	break;
                	 case "non":
                		 System.out.println("Merci d'avoir visiter notre site. Au revoir.");
                	break;	 
                	default:
                		System.err.println("Sorry, wrong input. Please try again.");
                	 }	 
                break;
                	
                case QUIT:
                    System.out.println("Bye...");
                    return;
                
                default:
                    System.err.println("Sorry, wrong input. Please try again.");
                    return;
            } 
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
	
	@Override
	protected void menu() {
		StringBuilder builder = new StringBuilder();
		builder.append(QUIT+". Quit.");
		builder.append("\n1. La liste des Agences Partenaire");
		builder.append("\n2. Consultation offre et Reservation Chambre");
		System.out.println(builder);		
	}
    
		
	}


