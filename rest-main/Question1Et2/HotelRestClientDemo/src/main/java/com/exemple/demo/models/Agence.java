package com.exemple.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Agence {
	
	private Long idP;
	private String pass_word;
	private String nomAgence;
	private boolean faitdemande;//afin de savoir quelle agence fait la reservation
	private float TarifPropre;//
	private float PourcentagePrix;//c'est un tarif par rapport au prix de base d'une chambre
	private String login;
	public Agence() {
		
	}
	public Agence( String pass_word,boolean faitdemande,float PourcentagePrix,String login,String nomAgence) {
		this.nomAgence=nomAgence;
		this.pass_word = pass_word;
		this.faitdemande=faitdemande;
		this.PourcentagePrix=PourcentagePrix;
		this.login=login;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public float getTarifPropre() {
		return TarifPropre;
	}

	public void setTarifPropre(float tarifPropre) {
		TarifPropre = tarifPropre;
	}

	public float getPourcentagePrix() {
		return PourcentagePrix;
	}

	public void setPourcentagePrix(float pourcentagePrix) {
		PourcentagePrix = pourcentagePrix;
	}

	public boolean isFaitdemande() {
		return faitdemande;
	}

	public void setFaitdemande(boolean faitdemande) {
		this.faitdemande = faitdemande;
	}
	public Long getIdP() {
		return idP;
	}
	public void setIdP(Long idP) {
		this.idP = idP;
	}
	public String getPass_word() {
		return pass_word;
	}
	public void setPass_word(String pass_word) {
		this.pass_word = pass_word;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public void setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
	}
	

}
