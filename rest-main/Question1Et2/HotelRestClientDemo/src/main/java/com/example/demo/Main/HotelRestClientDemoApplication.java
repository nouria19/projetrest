package com.example.demo.Main;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
"com.exemple.demo.models",
"com.exemple.demo.Client",
"com.exemple.demo.Cli"
})
public class HotelRestClientDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelRestClientDemoApplication.class, args);
	}

}
