package com.exemple.demo.data;


import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.exemple.demo.models.Agence;
import com.exemple.demo.models.Chambre;
import com.exemple.demo.models.Hotel;
import com.exemple.demo.models.Offre;
import com.exemple.demo.repository.AgenceRepository;
import com.exemple.demo.repository.ChambreRepository;
import com.exemple.demo.repository.HotelRepository;
import com.exemple.demo.repository.OffreRepository;



@Configuration
public class HotelData{
private Logger logger = LoggerFactory.getLogger(this.getClass());

@Bean
    public CommandLineRunner initDatabase(AgenceRepository repository1) {
    return args -> {//c'est les données à inserer à la base de données
    logger.info("Remplir la database avec "+ repository1.save(
    new Agence("agence20", false, 0.3f,"Log.agenceMona","MonaAgence")));
    logger.info("Remplir la database avec "+ repository1.save(
    new Agence("agence25", false,0.5f,"log.voyage","GoVoyage")));
    logger.info("Remplir la database avec "+ repository1.save(
    new Agence("agence51", false, 0.2f,"log.logi","LogiTravel")));
    };
}



@Bean
    public CommandLineRunner initDatabase1(ChambreRepository repository) throws URISyntaxException {
 SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
 URI url1= new URI("https://montdarbois.edmondderothschildheritage.com/wp-content/uploads/EDRH-Hotels-Page-Paysage-911x625_Chalets_Alpage.jpg");
 URI url2 = new URI("https://static.savoie-mont-blanc.com/wp-content/uploads/external/a1eb7feea20d112d388373e728cf60b6-12894517-diaporama-501x360.jpg");
 URI url3 = new URI("https://plagepalace.com/wp-content/uploads/2019/07/HOTEL_04_Plage-Palace.png");
 URI url4 = new URI("https://ak-d.tripcdn.com/images/220k190000016lo6r3669_Z_1100_824_R5_Q70_D.jpg");
 
    return args -> {//c'est les données à inserer à la base de données
   
    logger.info("Remplir la database avec "+ repository.save(new Chambre(5,3,163,sdformat.parse("2023-02-01"), true,url1)));
    logger.info("Remplir la database avec "+ repository.save(new Chambre(10,2,250,sdformat.parse("2023-05-25"), true,url2)));
    logger.info("Remplir la database avec "+ repository.save( new Chambre(2,4,350,sdformat.parse("2022-10-05"), true,url3)));
    logger.info("Remplir la database avec "+ repository.save( new Chambre(15,2,230,sdformat.parse("2022-12-10"), true,url4)));
    };
}
@Bean
    public CommandLineRunner initDatabase2(HotelRepository repository2, AgenceRepository repo,ChambreRepository repoCh) {
	List<Chambre> ch = new ArrayList<>();
	List<Agence> A = new ArrayList<>();
	return args -> {//c'est les données à inserer à la base de données
    	Agence agence =  repo.findById(1L).get();
    	Agence agence1 =  repo.findById(2L).get();
    	Agence agence2 =  repo.findById(3L).get();
    	
    	A.add(agence);
    	A.add(agence1);
    	A.add(agence2);
    
    	Chambre cha = repoCh.findById(1L).get();
    	Chambre cha1 = repoCh.findById(2L).get();
    	Chambre cha2 = repoCh.findById(3L).get();
    	Chambre cha3 = repoCh.findById(4L).get();
    	    	
    	ch.add(cha);
    	ch.add(cha1);
    	ch.add(cha2);
    	ch.add(cha3);
    	
    logger.info("Remplir la database avec "+ repository2.save(new Hotel("Alp_Hotel","France"," Megève"," Rochebrune",434," Rte de Rochebrune",15,2,A,ch)));
  
    };
}

}