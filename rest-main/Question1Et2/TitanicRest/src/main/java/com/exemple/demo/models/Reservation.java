package com.exemple.demo.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Entity
public class Reservation {
	@Id
	private String ref;
	private int nb_personnes;
	private Date dateA;
	private Date dateD;
	@OneToOne
	private Client client;
	@OneToOne
	private Chambre Chambres;
	@OneToOne
	private Agence Agence;//grâce à la classe d'association c'est l'agence qui fait la reservation pour le client 

	public Reservation() {
		
	}
	public Reservation(int nb_personnes, Date datA, Date dateD,String nom,String prenom,String infoCB,Agence agence,Client client) {
		super();
		this.nb_personnes = nb_personnes;
		this.dateA = datA;
		this.dateD = dateD;
		
		Agence = agence;
		this.client=client;
		this.Agence=agence;
		
	}
	
	/*public boolean FaireReservation (Chambre ch) {//une fois que l'agence à consulter les offre elle fait passer une chambre qui l'interresse 
		
		for(AgenceP t: Agence) {
			if(t.isFaitdemande() && ch.isEst_libre()) {
						t.setTarifPropre(t.getPourcentagePrix()* ch.getPrix());//si l'agence fait la reservation on met à jour son tarif selon son pourcenage et le prix de chambre
						ch.setEst_libre(false);//une fois la reservation effectuer la chambre n'est plus libre
						return true;
					}
			}
		return false;	
			}*/
	  
	public Reservation(Chambre chambre, String dateDebut, String dateFin, int nombrePersonnes,
			String infoCarteCreditClient,Client client,  Agence agence2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
		try {
			this.dateD=sdf.parse(dateDebut);
			this.dateA=sdf.parse(dateFin);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.client=client;
		this.nb_personnes=nombrePersonnes;
		
	}

	public Agence getAgence() {
		return Agence;
	}

	public void setAgence(Agence agence) {
		Agence = agence;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public int getNb_personnes() {
		return nb_personnes;
	}

	public void setNb_personnes(int nb_personnes) {
		this.nb_personnes = nb_personnes;
	}

	public Date getDatA() {
		return dateA;
	}

	public void setDatA(Date datA) {
		this.dateA = datA;
	}

	public Date getDateD() {
		return dateD;
	}

	public void setDateD(Date dateD) {
		this.dateD = dateD;
	}

	public Chambre getChambres() {
		return Chambres;
	}

	public void setChambres(Chambre chambres) {
		Chambres = chambres;
	}

}
