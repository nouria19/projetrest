package com.exemple.demo.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Offre {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	  private Long ident;
	@OneToOne
	  private Chambre chambre;
	    public Offre()
	    {}
		public Offre(Chambre offres) {
			this.chambre = offres;
		}
		public Long getIdentifiant() {
			return ident;
		}

		public void setIdentifiant(Long ident) {
			this.ident = ident;
		}

		public Chambre getChambre() {
			return chambre;
		}

		public void setChambre(Chambre chambre) {
			this.chambre = chambre;
		}
	    
	  
}
