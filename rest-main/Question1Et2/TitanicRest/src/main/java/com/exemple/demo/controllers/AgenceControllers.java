package com.exemple.demo.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RestController;

import com.exemple.demo.models.Agence;
import com.exemple.demo.models.Chambre;
import com.exemple.demo.models.Client;
import com.exemple.demo.models.Reservation;
import com.exemple.demo.repository.AgenceRepository;
import com.exemple.demo.repository.ChambreRepository;
import com.exemple.demo.repository.ClientRepository;
import com.exemple.demo.repository.OffreRepository;
import com.exemple.demo.repository.ReservationRepository;

//web service pour la réservation

@RestController
public class AgenceControllers {
	
	@Autowired
	AgenceRepository repo;
	@Autowired
	ClientRepository repClient;
	@Autowired
	OffreRepository repO;
	@Autowired
	ReservationRepository repR;
	@Autowired
	ChambreRepository repCh;
	
	private static final String uri ="/Titanic/Reservation/api";
	
	/* METHODS */
	@GetMapping(uri+"/Agences")
	public List<Agence> getAgence(){
	return repo.findAll();
	}
	@GetMapping(uri+"/Agence/{idP}")
	public Optional<Agence> getAgenceId(Long id) {
		return repo.findById(id);
	}
	//mise a jour de la table Agence dont id est idP
	@PutMapping(uri+"/Ajour/{idP}")	
	public Agence Ajour(@PathVariable Long idP) {
		Optional<Agence> agence=getAgenceId(idP);
		repo.findById(idP);
			agence.get().setFaitdemande(true);
			return repo.save(agence.get());		
	}
	
	@PutMapping(uri+"/sauvegarderReservation/{idP}/{identifiantOffre}/{dateA}/{dateD}/{NB_personne}/{nomC}/{prenomC}/{infoB}")
	public String sauvegarderReservation(@PathVariable long idP,@PathVariable long identifiantOffre,@PathVariable String dateA,@PathVariable String dateD,
			@PathVariable int NB_personne,@PathVariable String nomC,@PathVariable String prenomC,@PathVariable String infoB) {
		     Agence a = Ajour(idP);		     
		     
		     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    Client client = creerClient(nomC, prenomC,infoB);
	        Chambre chambre = repO.findById(identifiantOffre).get().getChambre();
	       
	        if (chambre == null)
	        {
	            return "PROBLEME : Erreur chambre invalide";
	        }
	        else  
	        {	  	        	
	        	if(a.isFaitdemande() && chambre.isEst_libre()) {
	        		
	        		Random _random = new Random();
			        int idReservation = _random.nextInt(300-100)+100;
		            Reservation reservation = new Reservation(chambre, dateA, dateD, NB_personne, infoB, client, a);
		            
		            reservation.setRef("#"+idReservation);
		            reservation.setChambres(chambre);
		            reservation.setAgence(a);
		           repR.save(reservation);
		            a.setTarifPropre(a.getPourcentagePrix()* chambre.getPrix());//si l'agence fait la reservation on met à jour son tarif selon son pourcenage et le prix de chambre
		            repo.save(a);
		            repR.save(reservation);
		            chambre.setEst_libre(false);
		            
		            try {
						chambre.setDateDispo(sdf.parse(dateD));
					} catch (ParseException e) {
						
						e.printStackTrace();
					}
		            repCh.save(chambre);
		            a.setFaitdemande(false);
		            repo.save(a);
		      
		            return  reservation.getRef();
	        	}
	        	return "La Chambre en Question n'est pas libre ou l'agence n'as pas fait aucune demande de réservation";
	        	
	        }
			
		}
	
	@GetMapping(uri+"/Reservation")
	public List<Reservation> getReservation(){
		return repR.findAll();
	}
	
	//@PostMapping(uri+"/creerClient/{nomClient}/{prenomClient}/{infoB}")
	public Client creerClient(@PathVariable String nomClient,@PathVariable String prenomClient,@PathVariable String infoB)
	{		
	    return repClient.save(new Client(nomClient, prenomClient,infoB));
	}

}
