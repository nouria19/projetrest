package com.exemple.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exemple.demo.models.Client;



public interface ClientRepository extends JpaRepository<Client,Long>{

}
