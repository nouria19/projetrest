package com.exemple.demo.models;

import java.text.ParseException;
import java.util.List;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Hotel {
	
	private Long idH;
	private String nomH;
	private String pays;
	private String ville;
	private String rue;
	private int numero;
	private String lieu_dit;
	private float longetude;
	private int NB_etoile;
	private List<Agence> Agences;
	private List<Chambre> ch;


	/* CONSTRUCTORS */
	public Hotel() {
		
	}
	public Hotel( String nomH,String pays, String ville, String rue, int numero, String lieu_dit, float longetude,
			 int nB_etoile,List<Agence> Agences,List<Chambre> ch) throws ParseException {
		this.nomH=nomH;
		this.pays = pays;
		this.ville = ville;
		this.rue = rue;
		this.numero = numero;
		this.lieu_dit = lieu_dit;
		this.longetude = longetude;
		NB_etoile = nB_etoile;
		this.Agences = Agences;
		this.ch = ch;		
	}
	
	/* METHODS */
	
	public Long getIdH() {
		return idH;
	}
	public void setIdH(Long idH) {
		this.idH = idH;
	}
	/*public List<Chambre> getCh() {
		return ch;
	}*/
	

	public String getNomH() {
		return nomH;
	}
	public void setNomH(String nomH) {
		this.nomH = nomH;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getLieu_dit() {
		return lieu_dit;
	}
	public void setLieu_dit(String lieu_dit) {
		this.lieu_dit = lieu_dit;
	}
	public float getLongetude() {
		return longetude;
	}
	public void setLongetude(float longetude) {
		this.longetude = longetude;
	}
	
	public int getNB_etoile() {
		return NB_etoile;
	}
	public void setNB_etoile(int nB_etoile) {
		NB_etoile = nB_etoile;
	}

	public List<Agence> getAgences() {
		return Agences;
	}

	public void setAgences(List<Agence> agences) {
		Agences = agences;
	}

	public List<Chambre> getChambres() {
		return ch;
	}

	public void setCh(List<Chambre> ch) {
		this.ch = ch;
	}


}
