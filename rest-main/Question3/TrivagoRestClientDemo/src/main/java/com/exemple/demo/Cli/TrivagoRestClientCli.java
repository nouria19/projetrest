package com.exemple.demo.Cli;

import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Map; 
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;


import com.fasterxml.jackson.*;
import com.exemple.demo.models.*;
import java.util.*;

@Component
public class TrivagoRestClientCli extends AbstractMain implements CommandLineRunner{
	
	/* ATTRIBUTES */
	@Autowired
	private RestTemplate proxy1;
	private static String URI_Trivago; 

	
	@Override
	protected boolean validServiceUrl() { 
	        	return SERVICE_URL.equals("http://localhost:8085/Trivago/Offres/api");
	        }

	
	@Override
	public void run(String... args) throws Exception {	
		//1er web service hotel consultation
		BufferedReader inputReader;
		String userInput = "";//c'est pour les cases 
		
		try {
		inputReader = new BufferedReader(
		new InputStreamReader(System.in));
		setTestServiceUrl(inputReader);
		URI_Trivago = SERVICE_URL;		
			
		do {
		menu();
		userInput = inputReader.readLine();
		
		processUserInput(inputReader, userInput,proxy1);
		Thread.sleep(3000);
		
		} while(!userInput.equals(QUIT));
		} catch (IOException e) {
		e.printStackTrace();
		} catch (InterruptedException e) {
		e.printStackTrace();
		}
		}
	
	public boolean valideLaDate(String laDate){
        if(laDate.length()==0){
            return false;
        }
        
        String dateFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);
        try {
            Date theDate = sdf.parse(laDate);
        } catch (java.text.ParseException e) {
            return false;
        }
        return true;
    }
	
	private void processUserInput(BufferedReader reader, 
            String userInput,RestTemplate proxy1) {
		
		Map<String, String> params = new HashMap<>();
		@SuppressWarnings("resource")
		Scanner scanner= new Scanner(System.in);
		
        try {
            switch(userInput) {
                case "1":
                	String uri = URI_Trivago +"/ListOffre/{ville}/{dateA}/{NB_personne}/{Nb_etoile}";            		
            		
                	System.out.println("Veuillez saisir la ville de l'hébérgement");
                	String ville = scanner.next();
                	params.put("ville", String.valueOf(ville));
                	
                	String dateA;
            		
            		do {
            		System.out.println("Veuillez saisir la date de votre arriver sous forme YYYY-MM-DD");
            		dateA = scanner.next();
            		params.put("dateA", String.valueOf(dateA));
            		}while(valideLaDate(dateA)==false);
            		
                	int NB_personne;
                	do {
                	System.out.println("Veuillez saisir le nombre de personne à héberger supérieur ou égal à 1");
                	NB_personne = scanner.nextInt();
            		params.put("NB_personne", String.valueOf(NB_personne));
                	}while(NB_personne <= 0);            		
            		
            		String dateD;
            		do {
            		System.out.println("Veuillez saisir la date de départ sous forme YYYY-MM-DD");
            		dateD = scanner.next(); 
            		params.put("dateD", String.valueOf(dateD));
            		}while(valideLaDate(dateD)==false);
                	
            		System.out.println("Veuillez saisir le nombre d'étoile pour l'hôtel que vous désirez");
            		int Nb_etoile = scanner.nextInt();
            		params.put("Nb_etoile", String.valueOf(Nb_etoile));
            		
            		Offre[] offre=proxy1.postForObject(uri,null,Offre[].class, params);
                	System.out.println("La liste d'offres est :");
                	Arrays.asList(offre);
                	 if(offre.length!=0) {
                	for(int i=0;i<offre.length;i++){
	                		 System.out.println("*************************************************************************************************");
	                		 System.out.println("                                                                                                 ");
	                		 System.out.println("Offre numero "+ offre[i].getIdent());
	                		 System.out.println("Le nom de l'hôtel :"+ offre[i].getNomH());
	                		 System.out.println("La ville de l'hôtel :"+ offre[i].getVille());
	                		 System.out.println("Le pays de l'hôtel :"+ offre[i].getPays());
	                		 System.out.println("L'adresse complète de l'hôtel :"+ offre[i].getNumero() +" "+ offre[i].getRue()+" "+offre[i].getLieu_dit());
	                		 System.out.println("Le nom de l'agence "+offre[i].getNomA());
	                		 System.out.println("Le nombre de lit de cette chambre est "+offre[i].getChambre().getNb_lits());
	                		 System.out.println("La Date de disponibilité de cette chambre est le  "+offre[i].getChambre().getDateDispo());
	                		 System.out.println("Le prix de cette chambre est "+offre[i].getChambre().getPrix());
	                		 System.out.println("                                                                                                 ");
	                		 System.out.println("*************************************************************************************************");
                	}}else {
                		   System.out.println("***************************************************************************************************");
		            	   System.out.println("                                                                                                   ");
		            	   System.err.println("Alert : Aucune des offres ne correspond à votre recherche veuillez verifier les informations saisie");
		            	   System.out.println("                                                                                                   ");
		            	   System.out.println("***************************************************************************************************");
		            	   break;
                	}
                	 
                	
                break;
                	
                case QUIT:
                    System.out.println("Bye...");
                    return;
                
                default:
                    System.err.println("Sorry, wrong input. Please try again.");
                    return;
            } }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
	
	@Override
	protected void menu() {
		StringBuilder builder = new StringBuilder();
		builder.append(QUIT+". Quit.");
		builder.append("\n1. Liste offres avec le nom des agences qui les proposes");
		System.out.println(builder);		
	}
    
		
	}


