package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
"com.exemple.demo.models",
"com.exemple.demo.Client",
"com.exemple.demo.Cli"
})
public class TrivagoRestClientDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrivagoRestClientDemoApplication.class, args);
	}

}
