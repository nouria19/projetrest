package com.exemple.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exemple.demo.models.Chambre;

public interface ChambreRepository extends JpaRepository<Chambre,Long> {

}
