package com.exemple.demo.exceptions;

public class AgenceNotFoundException extends AgenceException{
	public AgenceNotFoundException() {
	}
	public AgenceNotFoundException(String message) {
		super(message);
	}
}
