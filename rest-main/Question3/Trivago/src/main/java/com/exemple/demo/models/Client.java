package com.exemple.demo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Client {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idClient;
	private String nom;
	private String prenom;
	private String infoCB;
	
	public Client() {
	}
	
	public Client(String nom, String prenom, String infoCB) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.infoCB = infoCB;
	}

	public Client(String nomClient, String prenomClient) {
		this.nom=nomClient;
		this.prenom=prenomClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getInfoCB() {
		return infoCB;
	}

	public void setInfoCB(String infoCB) {
		this.infoCB = infoCB;
	}

	public long getIdClient() {
		return idClient;
	}

	public void setIdClient(long idClient) {
		this.idClient = idClient;
	}
}
