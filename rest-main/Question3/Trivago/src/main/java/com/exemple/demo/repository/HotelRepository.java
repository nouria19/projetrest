package com.exemple.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exemple.demo.models.Hotel;



public interface HotelRepository  extends JpaRepository<Hotel,Long> {

	
}
