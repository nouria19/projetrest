package com.exemple.demo.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Offre {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	  private Long ident;
	private String nomH;
	private int Nb_etoile;
	private String pays;
	private String ville;
	private String rue;
	private int numero;
	private String lieu_dit;
	@OneToOne
	private Chambre chambre;	
	private String nomA;
	
    
	public Offre()
    {}

	public Offre(String nomH,int nb_etoile, String pays, String ville,
			String rue, int numero, String lieu_dit,Chambre chambre,String nomA) {
		super();
		this.nomH = nomH;
		Nb_etoile = nb_etoile;
		this.pays = pays;
		this.ville = ville;
		this.rue = rue;
		this.numero = numero;
		this.lieu_dit = lieu_dit;
		this.chambre = chambre;		
		this.nomA = nomA;
		
	}

	public Long getIdent() {
		return ident;
	}

	public void setIdent(Long ident) {
		this.ident = ident;
	}

	public Chambre getChambre() {
		return chambre;
	}

	public void setChambre(Chambre chambre) {
		this.chambre = chambre;
	}

	public String getNomH() {
		return nomH;
	}

	public void setNomH(String nomH) {
		this.nomH = nomH;
	}

	public String getNomA() {
		return nomA;
	}

	public void setNomA(String nomA) {
		this.nomA = nomA;
	}

	public int getNb_etoile() {
		return Nb_etoile;
	}

	public void setNb_etoile(int nb_etoile) {
		Nb_etoile = nb_etoile;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getLieu_dit() {
		return lieu_dit;
	}

	public void setLieu_dit(String lieu_dit) {
		this.lieu_dit = lieu_dit;
	}
	
	    
	  
}
