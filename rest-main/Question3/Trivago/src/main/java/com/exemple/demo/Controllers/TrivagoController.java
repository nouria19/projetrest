package com.exemple.demo.Controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exemple.demo.exceptions.AgenceNotFoundException;
import com.exemple.demo.models.Agence;
import com.exemple.demo.models.Chambre;
import com.exemple.demo.models.Hotel;
import com.exemple.demo.models.Offre;
import com.exemple.demo.repository.AgenceRepository;
import com.exemple.demo.repository.HotelRepository;
import com.exemple.demo.repository.OffreRepository;

@RestController
public class TrivagoController {
	@Autowired //permet de faire de l'injection de dépendance
	private HotelRepository repository;
	private static final String uri ="/Trivago/Offres/api";
	@Autowired
    private OffreRepository rep;
	/*
	/* METHODS */
	@GetMapping(uri+"/hotels")
	public Hotel getHotel(){
	return repository.findAll().get(0);
	}
	
	@GetMapping(uri+"/Hotel/{idH}")
	public Optional<Hotel> getHotelId(Long id) {
		return repository.findById(id);
	}
	
	@DeleteMapping(uri+"/DeleteOffre")
	public void DeleteOffre() {
		rep.deleteAll();
	}
	
	
	@PostMapping(uri+"/ListOffre/{ville}/{dateA}/{NB_personne}/{Nb_etoile}")
 public List<Offre> ListOffre(@PathVariable String ville, @PathVariable String dateA,@PathVariable int NB_personne,@PathVariable int Nb_etoile) throws AgenceNotFoundException {
		
		if(rep.findAll().size() > 0) {
			DeleteOffre();
		}
		 SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			Date  d = sdformat.parse(dateA);
		List<Hotel> r = repository.findAll();
		for (int i=0;i<r.size();i++) {
			for (int j=0;j<r.get(i).getChambres().size();j++) {
				for (int k=0;k<r.get(i).getAgences().size();k++) {
					if((r.get(i).getVille()).equals(ville) && r.get(i).getNB_etoile() >= Nb_etoile && NB_personne <= r.get(i).getChambres().get(j).getNb_lits() && d.compareTo(r.get(i).getChambres().get(j).getDateDispo()) > 0
						) {
					Offre o = new Offre(r.get(i).getNomH(),r.get(i).getNB_etoile(),r.get(i).getPays(),r.get(i).getVille(),r.get(i).getRue(),r.get(i).getNumero(),r.get(i).getLieu_dit(),r.get(i).getChambres().get(j),r.get(i).getAgences().get(k).getNomAgence());
					rep.save(o);
				}	
		}
		}
		}
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		
   return rep.findAll();
		
	}

}
