package com.exemple.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exemple.demo.models.Agence;

public interface AgenceRepository  extends JpaRepository<Agence,Long> {

}
