package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages= {
"com.exemple.demo.models"
})
@EnableJpaRepositories(basePackages= {//pour savoir ou se trouve exactement le repository
"com.exemple.demo.repository"
})
//@EnableAutoConfiguration(exclude = BatchAutoConfiguration.class)
@SpringBootApplication(scanBasePackages = {
"com.exemple.demo.data",
"com.example.demo.exceptions",
"com.exemple.demo.Controllers"
})
public class TrivagoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrivagoApplication.class, args);
	}

}
